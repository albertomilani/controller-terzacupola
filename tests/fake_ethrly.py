#!/usr/bin/env python

from __future__ import division,print_function
import socket
import sys
import argparse
import struct

class FakeBoard:

    def __init__(self):
        self.relay = {i:False for i in xrange(8)}

    def parseCommand(self, c):
        if 1 <= (c-100) <= 8:
            self.relayOn(c-101)
            return None
        if 1 <= (c-110) <= 8:
            self.relayOff(c-111)
            return None
        if c == 91:
            return self.getStatuses()
        if c == 90:
            return self.getSoftwareVersion()
        if c == 93:
            return self.getInputVoltage()

    def relayOn(self, n):
        self.relay[n] = True

    def relayOff(self, n):
        self.relay[n] = False

    def getStatuses(self):
        s = sum([2**i*(1 if self.relay[i] else 0) for i in self.relay])
        return struct.pack('I', s)

    def getSoftwareVersion(self):
        return struct.pack('I', 8);

    def getInputVoltage(self):
        return struct.pack('I', 125);

    def printStatuses(self):
        print(', '.join([(str(i) + ' ' + ('ON' if s else 'OFF')) for i,s in self.relay.items()]))


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Fake EthRly Board')
    parser.add_argument('port_number', metavar='PORT', type=int, help='Server listening port')
    args = parser.parse_args()

    board = FakeBoard()

    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_TCP, socket.TCP_NODELAY, 1)
        sock.bind(('0.0.0.0', args.port_number))
        sock.listen(2)

        while True:
            conn, addr = sock.accept()

            try:
                data = conn.recv(4096)
                if data:
                    h, = struct.unpack('b', data)
                    ret = board.parseCommand(h)
                    if ret is not None:
                        conn.send(ret)
                    #board.printStatuses()
            except:
                pass

    except Exception, e:
        print('Error...')

    conn.close()
    sock.close()
