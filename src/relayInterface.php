<?php

error_reporting(E_ALL ^ E_STRICT);
ini_set('display_errors', true);

require('EthRly.inc.php');
require('logger.inc.php');

$logger = new Logger(Logger::LOG_LEVEL_INFO);

function computeDisabled($status, $board_id) {

  $disabled = array();
  for ($i=1; $i<=8; $i++) {
    $disabled[$i] = 0;
  }

  switch ($board_id) {
    case 0:
      break;
    case 1:
      if ($status[4] == 1) {
        $disabled[5] = 1;
      }
      if ($status[5] == 1) {
        $disabled[4] = 1;
      }
      if ($status[6] == 0) {
        $disabled[4] = 1;
        $disabled[5] = 1;
      }
      if ($status[7] == 1) {
        $disabled[8] = 1;
      }
      if ($status[8] == 1) {
        $disabled[7] = 1;
      }
      break;
    case 2:
      if ($status[1] == 1) {
        $disabled[2] = 1;
      }
      if ($status[2] == 1) {
        $disabled[1] = 1;
      }
      break;
    default:
      break;
  }
  return $disabled;
}

//$BOARD_0_IP = 'localhost';
//$BOARD_0_PORT = 17500;
//$BOARD_1_IP = 'localhost';
//$BOARD_1_PORT = 17501;

$BOARD_0_IP = '192.168.40.26';
$BOARD_0_PORT = 17494;
$BOARD_1_IP = '192.168.40.119';
$BOARD_1_PORT = 17494;
$BOARD_2_IP = '192.168.40.120';
$BOARD_2_PORT = 17494;

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
$logger->logDebug('RELAY-BOARD', 'Requested action: '.$action);
// validation
if (!$action) {
    echo json_encode(array('success' => false, 'message' => 'Action is required'));
    die;
}

if ($action == 'turn_all_relay_off') {
  // board 0
  $board = new EthRly($BOARD_0_IP, $BOARD_0_PORT);
  $board->connect();
  $board->turnAllRelayOff();
  // board 1
  $board = new EthRly($BOARD_1_IP, $BOARD_1_PORT);
  $board->connect();
  $board->turnAllRelayOff();
  // board 2
  $board = new EthRly($BOARD_2_IP, $BOARD_2_PORT);
  $board->connect();
  $board->turnAllRelayOff();

  echo json_encode(array('success' => true));
  die;
}


$board_id = $_REQUEST['board'];
$logger->logDebug('RELAY-BOARD', 'Requested board: '.$board_id);

switch ($board_id) {
  case 0:
    $board_ip = $BOARD_0_IP;
    $board_port = $BOARD_0_PORT;
    break;
  case 1:
    $board_ip = $BOARD_1_IP;
    $board_port = $BOARD_1_PORT;
    break;
  case 2:
    $board_ip = $BOARD_2_IP;
    $board_port = $BOARD_2_PORT;
    break;
}
$logger->logDebug('RELAY-BOARD', 'Board IP-PORT: '.$board_ip.'-'.$board_port);

$board = new EthRly($board_ip, $board_port);
$board->connect();

switch ($action) {
  
  case 'turn_relay_on':
    $relay = $_REQUEST['relay_num'];
    $logger->logInfo('RELAY-BOARD-ACTION', '[board '.$board_id.'] Turn on relay '.$relay);
    $board->turnRelayOn($relay);
    echo json_encode(array('success' => true));
    break;

  case 'turn_relay_off':
    $relay = $_REQUEST['relay_num'];
    $logger->logInfo('RELAY-BOARD-ACTION', '[board '.$board_id.'] Turn off relay '.$relay);
    $board->turnRelayOff($relay);
    echo json_encode(array('success' => true));
    break;

  case 'turn_all_relay_off':
    $logger->logInfo('RELAY-BOARD-ACTION', '[board '.$board_id.'] Turn off all relays');
    $board->turnAllRelayOff();
    echo json_encode(array('success' => true));
    break;

  case 'get_all_statuses':
    $logger->logDebug('RELAY-BOARD-ACTION', '[board '.$board_id.'] Get all statuses');
    $status = $board->getRelayStatus();
    $logger->logDebug('RELAY-BOARD-ACTION', '[board '.$board_id.'] Status: '.json_encode($status));
    $disabled = computeDisabled($status, $board_id);
    echo json_encode(array('success' => true, 'data' => $status, 'board' => $board_id, 'disable_buttons' => $disabled));
    break;

  case 'get_firmware':
    $logger->logDebug('RELAY-BOARD-ACTION', '[board '.$board_id.']Get firmware version');
    $version = $board->getBoardVersion();
    $logger->logDebug('RELAY-BOARD-ACTION', '[Board '.$board_id.'] Firware version: '.$version);
    echo json_encode(array('success' => true, 'version' => $version, 'board' => $board_id));
    break;

  case 'get_input_voltage':
    $logger->logDebug('RELAY-BOARD-ACTION', '[board '.$board_id.'] Get input voltage');
    $voltage = $board->getInputVoltage();
    $logger->logDebug('RELAY-BOARD-ACTION', '[board '.$board_id.'] Input voltage: '.$voltage);
    echo json_encode(array('success' => true, 'voltage' => $voltage, 'board' => $board_id));
    break;

  case 'get_ip_address':
    echo json_encode(array('success' => true, 'ip' => $board_ip, 'board' => $board_id));
    break;

}

