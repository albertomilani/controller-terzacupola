<?php

const CMD_GET_STATUS     = 'GET_STATUS';
const CMD_ROTATION_EAST  = 'ROT_EAST';
const CMD_ROTATION_OVEST = 'ROT_OVEST';

function sendCommand($command) {
    $socket = fsockopen('localhost', 23502, $errno, $errstr, 30);
    socket_set_timeout($socket, 1);
    if (!$socket) {
        return false;
    }
    fwrite($socket, $json);
    $res = fread($this->socket,1);
    $res = ord($res);
    fclose($socket);
    return $res;
}

$action    = $_REQUEST['action'];    # getstatus, doaction
$command   = $_REQUEST['command'];   # rotation
$direction = $_REQUEST['direction']; # east, ovest
$degree    = $_REQUEST['degree'];    # rotation degrees

switch($action) {
    case 'doaction':
        if ('rotation' == $command) {
            if ('east' == $direction) {
                $ctl_command = CMD_ROTATION_EAST;
            }
            if ('ovest' == $direction) {
                $ctl_command = CMD_ROTATION_OVEST;
            }
            $ctl_command .= '_'.$degree;
        }
        $res = sendCommand($ctl_command);
        echo json_encode(array('success' => ($res !== false)));
        die;
 
    case 'getstatus':
        $res = sendCommand(CMD_GET_STATUS);

        $status = array();

        echo json_encode(array('success' => true, 'data' => $status));
        die;

}

