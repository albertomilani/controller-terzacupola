<?php

include('logger.inc.php');

const CMD_GET_STATUS     = 'GET_STATUS';
const CMD_OPEN_SUP       = 'OPEN_SUP';
const CMD_OPEN_INF       = 'OPEN_INF';
const CMD_CLOSE_SUP      = 'CLOSE_SUP';
const CMD_CLOSE_INF      = 'CLOSE_INF';
const CMD_POWER_ON       = 'POWER_ON';
const CMD_POWER_OFF      = 'POWER_OFF';

const DANGER_VALUE = 2;

const LOW_ENCODER_POS_OPEN   = 16025624;
const LOW_ENCODER_POS_CLOSE  = 15933200;
const HIGH_ENCODER_POS_OPEN  = 15106944;
const HIGH_ENCODER_POS_CLOSE = 14903400;

$logger = new Logger(Logger::LOG_LEVEL_INFO);

// send command to plc server
function sendCommand($command) {
    $socket = fsockopen('192.168.40.115', 23501, $errno, $errstr, 30);
    socket_set_timeout($socket, 1);
    if (!$socket) {
        return false;
    }
    fwrite($socket, $command);
    return fread($socket,10);
}

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
$action = $_REQUEST['action'];

$logger->logDebug('DOME-SHUTTER-ACTION', 'Action requested: '.$action);

// validation
if (!$action) {
    echo json_encode(array('success' => false, 'message' => 'Action is required'));
    die;
}

switch($action) {
    case 'doaction':
        $command   = $_REQUEST['command']; # open, close
        $side      = $_REQUEST['side'];    # low, high

        $logger->logDebug('DOME-SHUTTER-ACTION', 'Command: '.$command.'. Side: '.$side);

        if ('open' == $command) {
            if ('high' == $side) {
                $ctl_command = CMD_OPEN_SUP;
            }
            if ('low' == $side) {
                $ctl_command = CMD_OPEN_INF;
            }
        }
        if ('close' == $command) {
            if ('high' == $side) {
                $ctl_command = CMD_CLOSE_SUP;
            }
            if ('low' == $side) {
                $ctl_command = CMD_CLOSE_INF;
            }
        }
        $logger->logDebug('DOME-SHUTTER-ACTION', 'Compute command: '.$ctl_command);

        $res = sendCommand($ctl_command);
        echo json_encode(array('success' => true));
        die;

    case 'setpower':
        $status = $_REQUEST['status'];
        if ($status == 'on') {
            $res = sendCommand(CMD_POWER_ON);
        } else {
            $res = sendCommand(CMD_POWER_OFF);
        }
        echo json_encode(array('success' => true));
        die;

    case 'getloggedusers':
        $users = ['alberto.milani'];
        echo json_encode(array('success' => true, 'users' => $users));
        die;

    case 'getstatus':
        $res = sendCommand(CMD_GET_STATUS);

        if ($res === false) {
            echo json_encode(array('success' => false, 'message' => 'ERROR: cannot connect to server'));
            die;
        }

        $array = unpack("Cbyte1/Cbyte2/Splc_status/C3enc1/C3enc2", $res);
    
        $status = array();
        $extra = array();

        // bytes 10-9-8
        $extra['position_encoder_high'] = ($array['enc23'] << 16 | $array['enc22'] << 8 | $array['enc21']);
        $extra['high_aperture_percentage'] = round(100 * (HIGH_ENCODER_POS_CLOSE - $extra['position_encoder_high']) / (HIGH_ENCODER_POS_CLOSE - HIGH_ENCODER_POS_OPEN), 0);

        // bytes 7-6-5
        $extra['position_encoder_low'] = ($array['enc13'] << 16 | $array['enc12'] << 8 | $array['enc11']);
        $extra['low_aperture_percentage'] = round(100 * (LOW_ENCODER_POS_CLOSE - $extra['position_encoder_low']) / (LOW_ENCODER_POS_CLOSE - LOW_ENCODER_POS_OPEN), 0);

        // bytes 3-4
        $extra['plc_status'] = ($array['plc_status'] & 0xFFFF);

        // byte 2
        $status['high_closed']           = intval(!($array['byte2'] & 0b00000001));
        $status['low_closed']            = intval(!(($array['byte2'] & 0b00000010) >> 1));
        $status['high_opened']           = intval(!(($array['byte2'] & 0b00000100) >> 2));
        $status['low_opened']            = intval(!(($array['byte2'] & 0b00001000) >> 3));
        $status['automatic']             = ($array['byte2'] & 0b00010000) >> 4;
        $status['manual']                = intval(!(($array['byte2'] & 0b00010000) >> 4));
        $status['pc_power_on']           = ($array['byte2'] & 0b00100000) >> 5;
        $status['arm_again_after_error'] = ($array['byte2'] & 0b01000000) >> 6;
        $status['busy']        = ($array['byte2'] & 0b10000000) >> 7;

        // byte 1
        $status['high_opening']      = $array['byte1'] & 0b00000001;
        $status['low_opening']       = ($array['byte1'] & 0b00000010) >> 1;
        $status['high_closing']      = ($array['byte1'] & 0b00000100) >> 2;
        $status['low_closing']       = ($array['byte1'] & 0b00001000) >> 3;
        $status['emergency_ok']      = ($array['byte1'] & 0b00010000) >> 4;
        $status['inverter1_ok']      = ($array['byte1'] & 0b00100000) >> 5;
        $status['inverter2_ok']      = ($array['byte1'] & 0b01000000) >> 6;
        $status['emergency_high_ok'] = ($array['byte1'] & 0b10000000) >> 7;


        // custom value emergency
        if (!$status['emergency_ok']) {
            $status['emergency_ok'] = DANGER_VALUE;
        }
        if (!$status['inverter1_ok']) {
            $status['inverter1_ok'] = DANGER_VALUE;
        }
        if (!$status['inverter2_ok']) {
            $status['inverter2_ok'] = DANGER_VALUE;
        }
        if (!$status['emergency_high_ok']) {
            $status['emergency_high_ok'] = DANGER_VALUE;
        }
        if ($status['arm_again_after_error']) {
            $status['arm_again_after_error'] = DANGER_VALUE;
        }
        if ($status['busy']) {
            $status['busy'] = DANGER_VALUE;
        }

        echo json_encode(array('success' => true, 'message' => '', 'data' => $status, 'extra' => $extra));
        die;

}
