<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Login 84cm</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>
   
    <div class="page-header">
      <div class="row">
        <div class="col-sm-offset-1 col-sm-6">
          <h1>Users on 84cm telescope</h1>
        </div>
      </div>
    </div>


    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3">

          <div class="form-group">
            <label>Username</label>
            <input class="form-control" type="text" id="username" name="username" />
          </div>
          <div class="form-group">
            <label>Password</label>
            <input class="form-control" type="password" id="password" name="password" />
          </div>
          <button class="btn btn-success" onclick="userLogin()">Login</button>
          <button class="btn btn-danger" onclick="userLogout()">Logout</button>
        </div>
        <div class="col-sm-3 col-md-offset-1">
          <h5>Logged users:</h5>
          <ul id="user_list">
          </ul>
        </div>
        <div class="col-sm-3 col-md-offset-1">
          <dl>
            <dt>Sunrise</dt>
            <dd><?php echo date_sunrise(time(), SUNFUNCS_RET_STRING, 45.8684, 8.77961); ?></dd>
            <dt>Sunset</dt>
            <dd><?php echo date_sunset(time(), SUNFUNCS_RET_STRING, 45.8684, 8.77961); ?></dd>
            <dt>Latitude</dt>
            <dd>45° 52' 06.4''</dd>
            <dt>Longitude</dt>
            <dd>8° 46' 14.2''</dd>
            <dt>Altitude</dt>
            <dd>1226m</dd>
          </dl>
        </div>


      </div>
    </div>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>

    <script>

      function userLogin() {
        var username = $('#username').val();
        var password = $('#password').val();

        console.log(username, password);
      }

      function getLoggedUsers() {
        var url = "domeShutterInterface.php?action=getloggedusers";
        $.ajax({url: url, success: function(res){
          result = JSON.parse(res);
          $('#user_list').empty();
          $.each(result.users, function(i) {
            var item = '<li>'+result.users[i]+'</li>';
            $('#user_list').append(item);
          });
        }});
      }

      setInterval(getLoggedUsers, 2000);

    </script>

  </body>

</html>
