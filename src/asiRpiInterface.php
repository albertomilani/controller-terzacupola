<?php

$ip_rpi[0] = '192.168.40.121';
$ip_rpi[1] = '192.168.40.122';
$ip_rpi[2] = '192.168.40.123';

function isOnline($ip) {
    $timeout = 1; 
    $options = array( 
          'http'=>array( 
            'method'=>"GET", 
            'header'=>"Accept-language: en\r\n", 
              'timeout' => $timeout 
              ) 
        ); 
    $context = stream_context_create($options); 
    $source = "http://".$ip."/index.html";
    $cc = file_get_contents($source, false, $context); 
    return (trim($cc) == "online");
}

function switchOff($ip) {
    $socket = fsockopen($ip, 20000, $errno, $errstr, 30);
    socket_set_timeout($socket, 1);
    if (!$socket) {
        return false;
    }
    $json = json_encode(array('command' => 'shutdown'));
    fwrite($socket, $json);
    fclose($socket);
    return true;
}

$action = $_REQUEST['action'];
$rpi_num = $_REQUEST['rpi_num'];

switch($action) {
    case 'isonline':
        $ip = $ip_rpi[$rpi_num];
        $isonline = isOnline($ip);
        echo json_encode(array('success' => true, 'rpi_num' => $rpi_num, 'isonline' => $isonline));
        die;

    case 'switchoff':
        $ip = $ip_rpi[$rpi_num];
        $ret = switchOff($ip);
        echo json_encode(array('success' => $ret, 'rpi_num' => $rpi_num));
        die;

}

