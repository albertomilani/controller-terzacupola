<?php

date_default_timezone_set('Europe/Rome');

class Logger {

    const LOG_FILE = '/var/log/terzacupola/terzacupola.log';

    const LOG_LEVEL_DEBUG = 10;
    const LOG_LEVEL_INFO  = 20;
    const LOG_LEVEL_WARN  = 30;
    const LOG_LEVEL_ERR   = 40;

    const LOG_LEVEL_DEBUG_NAME = 'INFO';
    const LOG_LEVEL_INFO_NAME  = 'DEBUG';
    const LOG_LEVEL_WARN_NAME  = 'WARN';
    const LOG_LEVEL_ERR_NAME   = 'ERROR';

    protected $log_level_name = array(10 => 'DEBUG', 20 => 'INFO', 30 => 'WARN', 40 => 'ERR');
    protected $min_log_level = self::LOG_LEVEL_DEBUG;

    public function __construct($min_log_level) {
        $this->min_log_level = $min_log_level; 
    }

    public function logInfo($short_log, $long_log) {
        $this->log(self::LOG_LEVEL_INFO, $short_log, $long_log);
    }

    public function logDebug($short_log, $long_log) {
        $this->log(self::LOG_LEVEL_DEBUG, $short_log, $long_log);
    }

    public function logWarning($short_log, $long_log) {
        $this->log(self::LOG_LEVEL_WARN, $short_log, $long_log);
    }

    public function logError($short_log, $long_log) {
        $this->log(self::LOG_LEVEL_ERR, $short_log, $long_log);
    }

    protected function log($level, $short_log, $long_log) {
        if ($level >= $this->min_log_level) {
            $this->writeToFile($level, $short_log, $long_log);
        }
    }

    protected function writeToFile($level, $short_log, $long_log) {
        list($decimals, $timestamp) = explode(" ", microtime());
        $string = date('Y-m-d\TH:i:s', $timestamp).".".substr($decimals, 2, 3)." ".$this->log_level_name[$level]." ".$short_log." -- ".$long_log."\n";
        file_put_contents(self::LOG_FILE, $string, FILE_APPEND);
    }
}

