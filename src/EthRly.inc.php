<?php

class EthRly {

  protected $ip;
  protected $port;
  protected $socket;

  public function __construct($ip, $port) {

    $this->ip = $ip;
    $this->port = $port;
  }

  public function __destruct() {

    fclose($this->socket);
  }

  public function connect() {

    if (ip2long($this->ip) === false && $this->ip != 'localhost') {
      $this->socket = false;
      return false;
    }
    $this->socket = fsockopen($this->ip, $this->port, $errno, $errstr, 30);
    socket_set_timeout($this->socket, 1);
    if (!$this->socket) {
      // errore
    }
  }

  protected function write ($command) {

    if (!$this->socket) {
      return false;
    }
    $out = pack('C', $command);
    $ret = fwrite($this->socket, $out);
    // leggo subito la risposta
    $res = fread($this->socket,1);
    $res = ord($res);

    $ret = fwrite($this->socket, "\x00");
    return $res;

  }

  public function turnRelayOn ($relay_num) {

    switch ($relay_num) {
      case 1:
        $command = 0x65;
        break;
      case 2:
        $command = 0x66;
        break;
      case 3:
        $command = 0x67;
        break;
      case 4:
        $command = 0x68;
        break;
      case 5:
        $command = 0x69;
        break;
      case 6:
        $command = 0x6A;
        break;
      case 7:
        $command = 0x6B;
        break;
      case 8:
        $command = 0x6C;
        break;
    }

    $res = $this->write($command);
    return true;
  }

  public function turnRelayOff ($relay_num) {

    switch ($relay_num) {
      case 1:
        $command = 0x6F;
        break;
      case 2:
        $command = 0x70;
        break;
      case 3:
        $command = 0x71;
        break;
      case 4:
        $command = 0x72;
        break;
      case 5:
        $command = 0x73;
        break;
      case 6:
        $command = 0x74;
        break;
      case 7:
        $command = 0x75;
        break;
      case 8:
        $command = 0x76;
        break;
    }

    $res = $this->write($command);
    return true;
  }

  public function turnAllRelayOn() {
    $res = $this->write(0x64);
    return true;
  }

  public function turnAllRelayOff() {
    $res = $this->write(0x6E);
    return true;
  }

  public function getRelayStatus() {
    $res = $this->write(0x5B);
    if ($res === false) {
      return array();
    }
    $status = array();
    $status[1] = $res & 0b00000001;
    $status[2] = ($res & 0b00000010) >> 1;
    $status[3] = ($res & 0b00000100) >> 2;
    $status[4] = ($res & 0b00001000) >> 3;
    $status[5] = ($res & 0b00010000) >> 4;
    $status[6] = ($res & 0b00100000) >> 5;
    $status[7] = ($res & 0b01000000) >> 6;
    $status[8] = ($res & 0b10000000) >> 7;
    return $status;
  }

  public function getBoardVersion() {
    $res = $this->write(0x5A);
    return $res;
  }

  public function getInputVoltage() {
    $res = floatval($this->write(0x5D))/10.0;
    return $res;
  }


}

