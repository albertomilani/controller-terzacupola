<?php

$labels[0][1] = "Switch 4-20mA lampada";
$labels[0][2] = "Accensione lampada Th";
$labels[0][3] = "Elettronica Newton";
$labels[0][4] = "Elettronica Cassegrain";
$labels[0][5] = "Motore DEC";
$labels[0][6] = "Motore AR";
$labels[0][7] = "Raspberry ASI";
$labels[0][8] = "Elettronica cupola e telescopio";

$labels[1][1] = "Board 2 - Relay 1";
$labels[1][2] = "Accensione blindo";
$labels[1][3] = "Accensione rotazione cupola";
$labels[1][4] = "Secondario Newton";
$labels[1][5] = "Secondario Cassegrain";
$labels[1][6] = "Sblocco secondari";
$labels[1][7] = "Chiudi Cassegrain";
$labels[1][8] = "Apri Cassegrain";

$labels[2][1] = "Apri tappo";
$labels[2][2] = "Chiudi tappo";
$labels[2][3] = "Alimentazione tappo";
$labels[2][4] = "Board 3 - Relay 4";
$labels[2][5] = "Board 3 - Relay 5";
$labels[2][6] = "Board 3 - Relay 6";
$labels[2][7] = "Board 3 - Relay 7";
$labels[2][8] = "Board 3 - Relay 8";

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Controller Relay 84cm</title>
    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
  </head>

  <body>

    <div class="page-header">
      <div class="row">
        <div class="col-sm-offset-1 col-sm-6">
          <h1>Controller 84cm</h1>
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-4">
          <p><b>Board 1</b></p>
          <p>Versione Firmware: <strong id="board_version_0">N/D</strong></p>
          <p>Input voltage: <strong id="board_voltage_0">N/D</strong></p>
          <p>Indirizzo IP: <strong id="board_ip_address_0">N/D</strong></p>
        </div>
        <div class="col-sm-4">
          <p><b>Board 2</b></p>
          <p>Versione Firmware: <strong id="board_version_1">N/D</strong></p>
          <p>Input voltage: <strong id="board_voltage_1">N/D</strong></p>
          <p>Indirizzo IP: <strong id="board_ip_address_1">N/D</strong></p>
	</div>
        <div class="col-sm-4">
          <p><b>Board 3</b></p>
          <p>Versione Firmware: <strong id="board_version_2">N/D</strong></p>
          <p>Input voltage: <strong id="board_voltage_2">N/D</strong></p>
          <p>Indirizzo IP: <strong id="board_ip_address_2">N/D</strong></p>
	</div>
      </div>

      <?php
        for ($i=1; $i<=8; $i++) {
          echo '<div class="row">';
	  for ($b=0; $b<3; $b++) {
            echo '  <div class="col-sm-2">';
            echo '    <div class="alert alert-warning" role="alert" id="relay_'.$b.'_'.$i.'"><strong>'.$labels[$b][$i].'</strong></div>';
            echo '  </div>';
            echo '  <div class="col-sm-2">';
            echo '    <div class="btn-group btn-group-lg" role="group">';
            echo '      <button type="button" class="btn btn-default" id="button_on_'.$b.'_'.$i.'" onclick="changeRelayState('.$b.', '.$i.', \'ON\');" disabled>ON</button>';
            echo '      <button type="button" class="btn btn-default" id="button_off_'.$b.'_'.$i.'" onclick="changeRelayState('.$b.', '.$i.', \'OFF\');">OFF</button>';
            echo '    </div>';
            echo '  </div>';
	  }
	  echo '</div>';
        }
      ?>

      <hr />

      <div class="row">
        <div class="col-sm-3">
          <h3>Controllo vani</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <p id="shutter_message"></p>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <h4>
            <span class="label label-default" id="automatic">Automatic</span>
            <span class="label label-default" id="manual">Manual</span>
          </h4>
        </div>
        <div class="col-sm-8">
          <h4>
            <span class="label label-default" id="emergency_ok">Emergenza OK</span>
            <span class="label label-default" id="emergency_high_ok">Emergenza Vano superiore OK</span>
            <span class="label label-default" id="inverter1_ok">Inverter 1 (inferiore) OK</span>
            <span class="label label-default" id="inverter2_ok">Inverter 2 (superiore) OK</span>
          </h4>
        </div>
        <div class="col-sm-8">
          <h4>
            <span class="label label-default" id="pc_power_on">Power ON</span>
            <span class="label label-default" id="arm_again_after_error">Necessario Riarmo Power ON</span>
            <span class="label label-default" id="busy">Busy</span>
          </h4>
        </div>
      </div>

      <br />

      <div class="row">
        <div class="col-sm-2">
          <p>
            Armo potenza quadro
          </p>
        </div>
        <div class="col-sm-8">
          <p>
            <button class="btn btn-default" type="button" onclick="domeShutterPower('on')">ARMO</button>
            <button class="btn btn-default" type="button" onclick="domeShutterPower('off')">DISARMO</button>
            (sono necessari circa 20 secondi per completare l'operazione)
          </p>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-2">
          <p>
            Vano Superiore
          </p>
        </div>
        <div class="col-sm-2">
          <p>
            <button class="btn btn-default" type="button" onclick="domeShutterAction('open', 'high')">APRI</button>
            <button class="btn btn-default" type="button" onclick="domeShutterAction('close', 'high')">CHIUDI</button>
          </p>
        </div>
        <div class="col-sm-6">
          <h4>
            <span class="label label-default" id="high_opening">In apertura</span>
            <span class="label label-default" id="high_closing">In chiusura</span>
            <span class="label label-default" id="high_opened">Aperto</span>
            <span class="label label-default" id="high_closed">Chiuso</span>
            <span>Apertura: <strong id="high_aperture_percentage">0</strong>%</span>
          </h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-2">
          <p>
            Vano Inferiore
          </p>
        </div>
        <div class="col-sm-2">
          <p>
            <button class="btn btn-default" type="button" onclick="domeShutterAction('open', 'low')">APRI</button>
            <button class="btn btn-default" type="button" onclick="domeShutterAction('close', 'low')">CHIUDI</button>
          </p>
        </div>
        <div class="col-sm-6">
          <h4>
            <span class="label label-default" id="low_opening">In apertura</span>
            <span class="label label-default" id="low_closing">In chiusura</span>
            <span class="label label-default" id="low_opened">Aperto</span>
            <span class="label label-default" id="low_closed">Chiuso</span>
            <span>Apertura: <strong id="low_aperture_percentage">0</strong>%</span>
          </h4>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-8">
          <p>Stato PLC: <strong id="plc_status">N/D</strong></p>
          <p>Posizione encoder superiore: <strong id="pos_enc_sup">N/D</strong></p>
          <p>Posizione encoder inferiore: <strong id="pos_enc_inf">N/D</strong></p>
        </div>
      </div>


      <hr />

      <div class="row">
        <div class="col-sm-8">
          <h3>Controllo Raspberry Pi</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <p>
            ASI ZWO Raspberry 1 (192.168.40.121) <span id="rpi_status_0" class="label label-warning">Unknown status</span>
            <button class="btn btn-default" type="button" onclick="turnOffRpi(0)">SPEGNI</button>
          </p>
          <p>
            ASI ZWO Raspberry 2 (192.168.40.122) <span id="rpi_status_1" class="label label-warning">Unknown status</span>
            <button class="btn btn-default" type="button" onclick="turnOffRpi(1)">SPEGNI</button>
          </p>
          <p>
            ASI ZWO Raspberry 3 (192.168.40.123) <span id="rpi_status_2" class="label label-warning">Unknown status</span>
            <button class="btn btn-default" type="button" onclick="turnOffRpi(2)">SPEGNI</button>
          </p>
        </div>
      </div>

    </div>

    <div id="switchOff" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Conferma spegnimento di TUTTI i relay</h4>
         </div>
      <div class="modal-body">
        <button type="button" class="btn btn-default" data-dismiss="modal" onClick="switchOff()">Conferma</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Annulla</button>
      </div>
    </div>

  </div>
</div>

    <!-- jQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="js/bootstrap.min.js"></script>

    <script>

      function getDomeShutterStatus() {
        var url = "domeShutterInterface.php?action=getstatus";
        $.ajax({url: url, success: function(res) {
          result = JSON.parse(res);
          if (result.success == false) {
            $("#shutter_message").text(result.message);
          } else { 
            $("#shutter_message").text(result.message);
            // status data
            for (var label in result.data) {
              var label_status = result.data[label];
              var tag_id = "#" + label;
              var tag = $(tag_id);
              tag.removeClass("label-default");
              tag.removeClass("label-success");
              tag.removeClass("label-danger");
              if (label_status == 2) {
                tag.addClass("label-danger");
              }
              if (label_status == 1) {
                tag.addClass("label-success");
              }
              if (label_status == 0) {
                tag.addClass("label-default");
              }
            }
            // extra info
            $("#plc_status").text(result.extra.plc_status);
            $("#pos_enc_sup").text(result.extra.position_encoder_high);
            $("#pos_enc_inf").text(result.extra.position_encoder_low);
            $("#low_aperture_percentage").text(result.extra.low_aperture_percentage);
            $("#high_aperture_percentage").text(result.extra.high_aperture_percentage);
          }
        }});
      }

      function domeShutterAction(action, side) {
        $.ajax({url: "domeShutterInterface.php?action=doaction&side="+side+"&command="+action, success: function(res){
          console.log("Side: "+side+", action: "+action);
        }});
      }

      function domeShutterPower(power_status) {
        $.ajax({url: "domeShutterInterface.php?action=setpower&status="+power_status, success: function(res){
          console.log("Power: "+power_status);
        }});
      }

      function domeRotate(direction) {
        var degree = $("#dome_rotation_degree").val();
        $.ajax({url: "domeInterface.php?action=doaction&command=rotation&direction="+direction+"&degree="+degree, success: function(res){
          console.log("Rotation "+direction+" for "+degree+" degrees");
        }});
      }

      function getRpiStatus() {
          for (var i=0; i<3; i++) {
              var url = "asiRpiInterface.php?action=isonline&rpi_num=" + i;
              $.ajax({url: url, success: function(res){
                result = JSON.parse(res);
                var tagid = "#rpi_status_" + result.rpi_num;
                var tag = $(tagid);
                tag.removeClass("alert-warning");
                if (result.isonline) {
                    tag.removeClass("alert-danger").addClass("alert-success");
                    tag.text('Online');
                } else {
                    tag.text('Offline');
                    tag.removeClass("alert-success").addClass("alert-danger");
                }
              }});
          }
      }

      function turnOffRpi(rpi_num){
        $.ajax({url: "asiRpiInterface.php?action=switchoff&rpi_num="+rpi_num, success: function(res){
          console.log("Spento rpi num",rpi_num);
        }});
      }

      function getStatus(){
        for (var i=0; i<3; i++) {
          var url = "relayInterface.php?action=get_all_statuses&board=" + i;
          $.ajax({url: url, success: function(res){
            result = JSON.parse(res);
            for (var relay_num in result.disable_buttons) {
              var tagid = "#button_on_" + result.board + "_" + relay_num;
              var tag = $(tagid);
              if (result.disable_buttons[relay_num] == 1) {
                tag.prop('disabled', true);
              } else {
                tag.prop('disabled', false);
              }
            }

            for (var relay_num in result.data) {
              var relay_status = result.data[relay_num];
              var tagid = "#relay_" + result.board + "_" + relay_num;
              var tag = $(tagid);
              tag.removeClass("alert-warning");
              if (relay_status == 1) {
                tag.removeClass("alert-danger").addClass("alert-success");
              }
              if (relay_status == 0) {
                tag.removeClass("alert-success").addClass("alert-danger");
              }
            }
          }});
        }
      }

      function getInputVoltage(){
        for (var i=0; i<3; i++) {
          $.ajax({url: "relayInterface.php?action=get_input_voltage&board=" + i, success: function(res){
            result = JSON.parse(res);
            $("#board_voltage_" + result.board).text(result.voltage);
          }});
        }
      }

      function getIpAddress(){
        for (var i=0; i<3; i++) {
          $.ajax({url: "relayInterface.php?action=get_ip_address&board=" + i, success: function(res){
            result = JSON.parse(res);
            $("#board_ip_address_" + result.board).text(result.ip);
          }});
        }
      }

      function switchOff(){
        $.ajax({url: "relayInterface.php?action=turn_all_relay_off", success: function(res){
          console.log("SPENTO TUTTO!");
        }});
      }

      function changeRelayState(board, relay, state) {
        console.log(board);
        console.log(relay);
        console.log(state);
        switch (state) {
          case 'ON':
            var method = 'turn_relay_on';
            break;
          case 'OFF':
            var method = 'turn_relay_off';
            break;
        }
        var url = "relayInterface.php?action=" + method + "&board=" + board + "&relay_num=" + relay;
        $.ajax({url: url, success: function(res){
         result = JSON.parse(res);
        }});
      }

      $( document ).ready(function() {
        console.log("get firmware version");
        for (var i=0; i<3; i++) {
          var url = "relayInterface.php?action=get_firmware&board=" + i;
          $.ajax({url: url, success: function(res){
            result = JSON.parse(res);
            $("#board_version_" + result.board).text(result.version);
          }});
        }

        getIpAddress();
        getInputVoltage();

        setInterval(getStatus, 1000);
        setInterval(getInputVoltage, 5000);
        setInterval(getRpiStatus, 1500);
        setInterval(getDomeShutterStatus, 2000);

      });
    </script>

  </body>

</html>
